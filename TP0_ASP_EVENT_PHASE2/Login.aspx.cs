﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using TP0_ASP_EVENT_PHASE2.classes;

namespace TP0_ASP_EVENT_PHASE2
{
    public partial class Login : System.Web.UI.Page
    {
        string connection;
        protected void Page_Load(object sender, EventArgs e)
        {
            connection = WebConfigurationManager.ConnectionStrings["connection"].ConnectionString;

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            LogIn login = new LogIn(connection);
            string[] res = login.checkUser(txtEmail.Value, txtPassword.Value);
            if (res == null)
            {
                Response.Redirect("Login.aspx");
            }
            else if(res[1]=="admin")
            {
                Session["admin"] = res[0];
                HttpCookie username = new HttpCookie("username");
                username.Value = res[2].ToString();
                HttpCookie image = new HttpCookie("image");
                image.Value = res[4].ToString();
                Response.Cookies.Add(username);
                Response.Cookies.Add(image);
                Response.Redirect("Admin.aspx");
            }
            else
            {
                Session["client"] = res[0];
                Response.Redirect("pages/Invitation_page.aspx");
            }

        }
    }
}
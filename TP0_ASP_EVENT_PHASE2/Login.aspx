﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TP0_ASP_EVENT_PHASE2.Login" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap Simple Login Form</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script> 
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }   
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
    body{
        background: url('../img/rethink-target-heart-rate-number-ftr.jpg') fixed;
    }
</style>
</head>
<body>
<div class="login-form">
    <form runat="server">
        <h2 class="text-center">Log in</h2>       
        <div class="form-group">
            <input type="email"  runat="server" ID="txtEmail" class="form-control" placeholder="Email" required="required">
        </div>
        <div class="form-group">
            <input type="password" runat="server" id="txtPassword" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <asp:Button Text="Log in" CssClass="btn btn-primary btn-block" ID="btnLogin" OnClick="btnLogin_Click" runat="server" />
        </div>
        <div class="clearfix">
            <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
            <a href="#" class="pull-right">Forgot Password?</a>
        </div>        
    </form>
    <p class="text-center" ><a href="#">Create an Account</a></p>
</div>
</body>
</html>                                		                            

﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace TP0_ASP_EVENT_PHASE2
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            Application["visiteurs_online"] = 0;
            Application["total_visiteurs"] = 0;

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Timeout = 5;
            Application.Lock();
            Application["visiteurs_online"] = int.Parse(Application["visiteurs_online"].ToString()) + 1;
            Application["total_visiteurs"]= int.Parse(Application["total_visiteurs"].ToString()) + 1;
            Application.UnLock();
            
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            Application.Lock();
            Application["visiteurs_online"] = int.Parse(Application["visiteurs_online"].ToString()) - 1;
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
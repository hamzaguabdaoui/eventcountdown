﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TP0_ASP_EVENT_PHASE2
{
    public partial class Messages1 : System.Web.UI.Page
    {
        SqlConnection connection;
        DataTable table;
        protected void Page_Load(object sender, EventArgs e)
        {
            string strCnx = WebConfigurationManager.ConnectionStrings["connection"].ConnectionString;
            connection = new SqlConnection(strCnx);
            string query = "select * from messages order by status asc";
            SqlDataAdapter adapter = new SqlDataAdapter(query,connection);
            table = new DataTable();

            connection.Open();
                adapter.Fill(table);
            connection.Close();

            foreach (DataRow row in table.Rows)
            {
                int status = int.Parse(row["status"].ToString());
                string etat = "";
                if (status == 0)
                    etat = "unread";
                string message = (row["sujet"].ToString()).Substring(0,30);
                DateTime date = DateTime.Parse(row["dateMessage"].ToString());
                
                string ligne = "<tr  class='" + etat + " rowC' data-id='"+row["idMessage"]+"'>"+
                                    "<td class='view-message  dont-show'>"+row["nom"]+"</td>" +
                                    "<td class='view-message  dont-show'>" + row["email"] + "</td>" +
                                    "<td class='view-message  dont-show'>" + row["objet"] + "</td>" +
                                    "<td class='view-message  dont-show'>" + message + "</td>" +
                                    "<td class='view-message  dont-show'>" + date.ToShortDateString() + "</td>"+
                                    "<td class='view-message  dont-show'>" + date.ToShortTimeString() + "</td>";
                messages.InnerHtml += ligne;
            }
        }
    }
}
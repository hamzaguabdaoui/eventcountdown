﻿<%@ Page Language="C#" MasterPageFile="~/admin_master.Master" AutoEventWireup="true" CodeBehind="ShowMessage.aspx.cs" Inherits="TP0_ASP_EVENT_PHASE2.Messages" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="page-container">
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <p class="h3">Messages Manager</p>
                    <asp:HiddenField   runat="server" Value="" ID="txtId" />
                    <div class="bg-light p-5">
                        <dl class="row">
                            <dt class="col-sm-3">From</dt>
                            <dd runat="server" id="txtFrom" class="col-sm-9">Amin</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3">Email:</dt>
                            <dd runat="server" id="txtEmail" class="col-sm-9">amin@emaik.com</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3">Object:</dt>
                            <dd runat="server" id="txtObjet" class="col-sm-9">amin@emaik.com</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3">Message:</dt>
                            <dd runat="server" id="txtMessage" class="col-sm-9">Additionally, Bootstrap also includes an .mx-auto class for horizontally centering fixed-width block
                                level content—that is, content that has display: block and a width set—by setting the horizontal margins to auto.
                            </dd>
                        </dl>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Delete</button>

                        <div id="deleteModal" class="modal" tabindex="-1" data-backdrop="false" role="dialog">
                            <div class="modal-dialog" style="margin-top: 120px;">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <p class="h4">Delete confirmation</p>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you realy want to delete this message.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="deletMessage" class="btn btn-success">Confirme</button>
                                        <button type="button" id="closeModal" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                     <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright ©2019 Agora ACI.</p>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="scripts" runat="server">
    <script>
        $('document').ready(onReady);

        function onReady() {
            $("#deletMessage").click(function () {
                let id = document.getElementById('<%= txtId.ClientID %>').value;
                PageMethods.deleteMessage(id, onSuccess);
                function onSuccess(data) {
                    if (data > 0) {
                        $('#closeModal').click();
                        window.location.href = "Admin.aspx";
                    }
                }
            });
        }
    </script>
</asp:Content>


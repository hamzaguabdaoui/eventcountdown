﻿<%@ Page Language="C#" MasterPageFile="~/pages/layout.Master" AutoEventWireup="true" CodeBehind="Accueil.aspx.cs" Inherits="TP0_ASP_EVENT_PHASE2.pages.Accueil" %>



<asp:Content runat="server" ContentPlaceHolderID="bodyContent">
     <!--DEBUT SECTION COVER-->
    <section class="cover container-fluid">
          <div id="eventInfo" class="row">
            <div class='col-lg-12 text-center'>
              <h2>23<sup>éme </sup>AGORA CLUB <br />INRENATIONALE </h2>
                <h1>CONFERENCE <br />REUNION <span id="number2">2</span><span id="number0">0</span><span id="number1">1</span>9</h1>
                <p>Marrakech - Maroc 3-6 octobre 2019 .</p>
            </div>
            <div class="countDown text-center col-lg-12">
               <h4>L'événement va commencer apres:</h4>
               <p id="Timer"></p>
                <asp:Button id="informations" CssClass="btn btn-success btn-md" Text="Plus d'information"  runat="server" OnClick="informations_Click" CausesValidation="false"/>
            </div>
          </div>  
    </section>
    <main id='main'>
      <section id="about">
        <div class="container-fluid bg-3 text-center">    
            <div class="row row-eq-height">
              <div class="col-sm-6 col-height" style="text-align: left !important;">
                <h1>rendez-vous à marrakech</h1>
                <p>L'Agora Club Marrakech accueille 3 octobre 2019 plus de 200 dames de 18 pays différents pour la conférence annuelle de l'Agora Club International, qui se tiendra au plusieurs ville marocaine. Le club local a été créé en 2014 et compte actuellement plus de 30 membres.
                Un programme varié d’activités, comprenant des visites de l’île et des événements sociaux.
                Un groupe d'anciens membres de Ladies Circle a créé le premier Agora Club à Lille, en France, en 1987. L'année suivante, le Conseil national français était fondé. Des clubs ont alors commencé à se former aux Pays-Bas (1993) et en Belgique (1994). Le premier conseil international a eu lieu à Strasbourg, en France, en 1996.
                Aujourd'hui, Agora Club International compte des clubs en Autriche, Belgique, Botswana, France, Allemagne, Islande, Israël, Italie, Pays-Bas, Madagascar, Malte, Maroc, Norvège, Roumanie, Afrique du Sud, Suisse, Royaume-Uni, États-Unis et Zambie.
                De nouveaux clubs et de nouveaux pays continuent de manifester de l'intérêt
                Pour plus d'informations, visitez le site www.agoraclubinternational.com.</p>
              </div>
              <div class="col-sm-6"> 
                <img src="../img/ACI-2019-Marakesh.jpg" class="img-responsive margin" alt="Image">
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-6 col-height"> 
                <img src="../img/logo_anna.png" class="img-responsive margin"  alt="Image">
              </div>
              <div class="col-sm-6 col-height"  style="text-align: left !important;">
                <!-- DEBUT FAQ COLLAPS-->
                  <div class="panel-group" id="accordion">
                    <div class="faqHeader">
                        <h1>FAQ</h1>
                        <hr>
                        <h2>Questions</h2>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">Agora Club International?</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                Le Club AGORA  est un club service féminin créé en 1987. L’emblème du Club est la feuille du GINKGO BILOBA, l’arbre qui a vaincu le temps. Sa couleur est le vert, symbole de vigueur,Action,Générosité,Ouverture d’esprit,Respect des différences et Amitié.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse2">Membres?</a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse3">Organisation?</a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse ">
                            <div class="panel-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse4">Services?</a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            </div>
                        </div>
                    </div>
                  </div>
                <!-- FIN FAQ COLLAPS-->
              </div>
            </div>
          </div>
          

      </section>
      
      <div id="mySlider">
          <div  class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="../img/background01.jpg" alt="Los Angeles" style="width:100%;">
              </div>

              <div class="item">
                <img src="../img/background02.jpg" alt="Chicago" style="width:100%;">
              </div>
            
              <div class="item">
                <img src="../img/background03.jpg" alt="New york" style="width:100%;">
              </div>
            </div>
          </div>
      </div>
      <br>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h3>OUR TEAM</h3>
          </div>
          <div class="col-md-12 text-center">
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <hr>
            </div>
          </div>
        </div>
      </div>
      <div id="team" class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="card profile-card-4">
                <div class="card-img-block">
                    <div class="info-box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                    <img class="img-fluid" src="../img/background01.jpg" alt="Card image cap">
                </div>
                <div class="card-body pt-5">
                    <img src="../img/3.jpg" alt="profile-image" class="profile"/>
                    <h5 class="card-title text-center">Katie Perse</h5>
                    <p class="card-text text-center">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <div class="icon-block text-center"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"> <i class="fa fa-twitter"></i></a><a href="#"> <i class="fa fa-google-plus"></i></a></div>
                </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card profile-card-4">
                <div class="card-img-block">
                    <div class="info-box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                    <img class="img-fluid" src="../img/background01.jpg" alt="Card image cap">
                </div>
                <div class="card-body pt-5">
                    <img src="../img/5.jpg" alt="profile-image" class="profile"/>
                    <h5 class="card-title text-center">John Merchel</h5>
                    <p class="card-text text-center">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <div class="icon-block text-center"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"> <i class="fa fa-twitter"></i></a><a href="#"> <i class="fa fa-google-plus"></i></a></div>
                </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card profile-card-4">
                <div class="card-img-block">
                    <div class="info-box">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                    <img class="img-fluid" src="../img/background01.jpg">
                </div>
                <div class="card-body pt-5">
                    <img src="../img/14.jpg" alt="profile-image" class="profile"/>
                    <h5 class="card-title text-center">Gail Schmidt</h5>
                    <p class="card-text text-center">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <div class="icon-block text-center"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"> <i class="fa fa-twitter"></i></a><a href="#"> <i class="fa fa-google-plus"></i></a></div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <br><br>
      <div class="container text-center">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <br><br>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h3>OUR PARTNERS</h3>
          </div>
          <div class="col-md-12 text-center">
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <hr>
            </div>
          </div>
        </div>
      </div>
      <br><br>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat.</blockquote>
          </div>
          <div class="col-md-6">
            <div class="col-md-12">
              <div class="col-md-3"><img src="../img/sultan.png"></div>
              <div class="col-md-3"><img src="../img/M6_logo.png"></div>
              <div class="col-md-3"><img src="../img/logo2x.png"></div>
              <div class="col-md-3"><img src="../img/logo-banque.png"></div>
            </div>
            <div class="clearfix"></div>
            <br><br>
            <div class="col-md-12">
              <div class="col-md-3"><img src="../img/France_2.png"></div>
              <div class="col-md-3"><img src="../img/2M_Logo.png"></div>
              <div class="col-md-3"><img src="../img/real_istat.jpg"></div>
              <div class="col-md-3"><img src="../img/TV5Monde_logo.png"></div>
            </div>
          </div>
        </div>
      </div>
      <br><br>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h3>CONTACT US</h3>
          </div>
          <div class="col-md-12 text-center">
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <hr>
            </div>
          </div>
        </div>
      </div>
      <br><br>
      <div id="contact" class="container">
          <div class="success">
              <asp:Label Text="text" ID="lblMessage" runat="server" />
          </div>
        <div class="row">
          <div class="col-md-4">
              <div class="form-group">
                <asp:TextBox runat="server" ID="txtNom" CssClass="form-control" placeholder="Entrez votre nom..."></asp:TextBox>
              </div>
              <asp:RequiredFieldValidator runat="server" ForeColor="Red" ErrorMessage="*champs obligatoir"
                  ControlToValidate="txtNom" />
              <div class="form-group">
                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" placeholder="Entrez votre email..."></asp:TextBox>
              </div>
              <asp:RegularExpressionValidator runat="server" 
                ControlToValidate="txtEmail"
                ErrorMessage="*l'email n'est pas valide \n exemple:test@test.ma"
                ForeColor="Red"
                ValidationExpression="^([A-Za-z0-9-_.]){2,}\@([A-Za-z]){4,}\.([A-Za-z]){2,4}$"
                />
              <div class="form-group">
                <asp:TextBox runat="server" ID="txtObjet" CssClass="form-control" placeholder="Entrez le titre de votre message..."></asp:TextBox>
              </div>
              <asp:RequiredFieldValidator runat="server" ForeColor="Red" ErrorMessage="*champs obligatoir"
                  ControlToValidate="txtObjet" />
              <div class="form-group">
                <asp:TextBox runat="server" ID="txtMessage" TextMode="MultiLine" CssClass="form-control" placeholder="Entrez votre message..."></asp:TextBox>
              </div>
              
              <div class="form-group">
                <asp:Button runat="server" ID="btnEnvoyer"  CssClass="btn btn-success" Text="Envoyer votre message" OnClick="btnEnvoyer_Click"></asp:Button>
              </div>
              <asp:RequiredFieldValidator runat="server" ForeColor="Red" ErrorMessage="*champs obligatoir"
                  ControlToValidate="txtMessage" />
              
          </div>
          <div class="col-md-5">
            <div id="map-container-google-1" class="z-depth-1-half map-container" style="height: 400px">
              <iframe src="https://maps.google.com/maps?q=Four%Seasons%Resort%Marrakech, Marrakesh&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
          <div class="col-md-3">
            <div class="col-md-12">
              <h4>Agora 23 2019</h4>
            </div>
            <div class="col-md-12">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-12">
              <div class="col-md-3"><i class="fa fa-map-marker" style="font-size: 60px; color: #E55"></i></div>
              <div class="col-md-9"><strong>Adresse</strong><br>Avenue de la Ménara, Marrakesh 40150</div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <div class="col-md-3"><i class="fa fa-mobile" style="font-size: 60px; color: #E55"></i></div>
              <div class="col-md-9"><strong>Télephone</strong><br>(+212)6 61 31 95 02</div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <div class="col-md-3"><i class="fa fa-envelope" style="font-size: 40px; color: #E55"></i></div>
              <div class="col-md-9"><strong>E-mail</strong><br>aci2019marrakech@gmail.com</div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
        
      <br><br>
</asp:Content>
<asp:content runat="server" ContentPlaceHolderID="scripts">
    <script type="text/javascript" src="../js/script.js"></script>
</asp:content>

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TP0_ASP_EVENT_PHASE2.pages
{
    public partial class Accueil : System.Web.UI.Page
    {

        static SqlConnection connection; 
        protected void Page_Load(object sender, EventArgs e)
        {
            string strCnx = WebConfigurationManager.ConnectionStrings["connection"].ConnectionString;
            connection = new SqlConnection(strCnx);
        }

        protected void btnEnvoyer_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "insert into messages values(@nom,@email,@objet,@sujet)";
            cmd.Connection = connection;
            SqlParameter[] parameters =
            {
                new SqlParameter("nom",txtNom.Text),
                new SqlParameter("email",txtEmail.Text),
                new SqlParameter("objet",txtObjet.Text),
                new SqlParameter("sujet",txtMessage.Text)
            };
            cmd.Parameters.AddRange(parameters);
            connection.Open();
            int res = cmd.ExecuteNonQuery();
            connection.Close();
            if (res > 0)
            {
                lblMessage.Text = "Votre Message a été envoyée";
                txtEmail.Text = "";
                txtMessage.Text = "";
                txtNom.Text = "";
            }
            else
            { 
                lblMessage.Text = "Une Erreur est servenu lors de l'envoi du message";
            }
        }

        public static string[] getStatistics()
        {
            string[] data=new string[5];

            return data;

        }

        protected void informations_Click(object sender, EventArgs e)
        {
            Response.Redirect("Schedule.aspx");
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/pages/layout.Master" CodeBehind="Schedule.aspx.cs" Inherits="TP0_ASP_EVENT_PHASE2.pages.Schedule" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
    <link href="../css/schedule.css" rel="stylesheet" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="bodyContent">
    <section id="schedule" class="section-with-bg">
        <div class="container wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
            <div class="section-header">
                <h2>ACI PROGRAMME</h2>
                <p>Welcome to our schedule!</p>
            </div>
            <div class="schedule-body">
                <div class="menu-programme" runat="server">
                    <ul runat="server" id="menuProgrammeList" class="nav nav-tabs" role="tablist">
                        <li class="nav-item">

                            <a class="nav-link days active show" data-id="1" href="#day-1" role="tab" data-toggle="tab" aria-selected="false">
                                <asp:Label Text="day 2" ID="day1" runat="server" /></br>
                                <asp:Label Text="23-octiner-2018" ID="day1Date" runat="server" /></br>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link days" href="#day-2" data-id="2" role="tab" data-toggle="tab" aria-selected="false">
                                <asp:Label Text="" ID="day2" runat="server" /></br>
                                <asp:Label Text="" ID="day2Date" runat="server" /><br>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link days " href="#day-3" data-id="3" role="tab" data-toggle="tab" aria-selected="true">
                                <asp:Label Text="" ID="day3" runat="server" /><br/>
                                <asp:Label Text="" ID="day3Date" runat="server" /><br />
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link days " href="#day-4" data-id="4" role="tab" data-toggle="tab" aria-selected="true">
                                <asp:Label Text="" ID="day4" runat="server" /><br/>
                                <asp:Label Text="" ID="day4Date" runat="server" /><br/>
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="programm_details">
                    <div class="tab-content row justify-content-center">
                        <div class="row">
                            <div class="col-md-6">
                                <div style="display: inline-block; padding-top: 15px;">
                                    <h4 runat="server" id="TimeActivity" class="time">09:30 -    16:00 </h4>
                                    <h3 runat="server" id="TitleActivity" class="title">MARRAKESH MEDINA, SOUKS,<br>
                                        COUNCILLORS MEETING, WELCOME<br>
                                        PARTY</h3>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <img runat="server" class="activity-image" id="activityImage" src="../img/agora.png" alt="Alternate Text" />
                            </div>
                            <div class="clearfix"></div>

                        </div>

                    </div>
                    <div runat="server" id="myDiv">
                    </div>
                </div>

            </div>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
            <div>
                <div class="center">
                    <b>Start: </b>
                    <select class="form-control form-control-sm startTraget" id="start" onchange="document.getElementById('map').src = this.options[this.selectedIndex].value">
                        <option value="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d27180.839232824903!2d-8.009642239575511!3d31.617284230596105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0xdafeed09eb0c575%3A0xc3f5815b7b3d9fe7!2sMarrakesh+Menara+Airport!3m2!1d31.604788099999997!2d-8.0210505!4m5!1s0xdafee8d96179e51%3A0x5950b6534f87adb8!2sMarrakesh!3m2!1d31.6294723!2d-7.9810845!5e0!3m2!1sen!2sma!4v1553479086221">Airport</option>
                        <option value="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d22853.731035855133!2d-8.011770807973095!3d31.627622418208137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e0!4m5!1s0xdafee9465ae0bf7%3A0x27332ef8ec3bea85!2sMarrakech!3m2!1d31.629793!2d-8.019043!4m5!1s0xdafee8d96179e51%3A0x5950b6534f87adb8!2sMarrakesh!3m2!1d31.6294723!2d-7.9810845!5e0!3m2!1sen!2sma!4v1553479229589">Train Station</option>
                    </select>
                </div>
                <iframe style="width: 100%;" src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d27180.839232824903!2d-8.009642239575511!3d31.617284230596105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0xdafeed09eb0c575%3A0xc3f5815b7b3d9fe7!2sMarrakesh+Menara+Airport!3m2!1d31.604788099999997!2d-8.0210505!4m5!1s0xdafee8d96179e51%3A0x5950b6534f87adb8!2sMarrakesh!3m2!1d31.6294723!2d-7.9810845!5e0!3m2!1sen!2sma!4v1553479086221" id="map" width="600" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>


            </div>
        </div>

    </section>
</asp:Content>
<asp:Content ContentPlaceHolderID="scripts" runat="server">
    <script src="../js/ScheduleAjax.js">

    </script>
</asp:Content>

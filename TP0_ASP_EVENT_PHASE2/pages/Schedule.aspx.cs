﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace TP0_ASP_EVENT_PHASE2.pages
{
    public partial class Schedule : System.Web.UI.Page
    {
        static SqlConnection cnx;
        SqlDataAdapter adapterProgramme, adapterProgrammeDetails;
        static DataTable programmes, programme_details;


        protected void Page_Load(object sender, EventArgs e)
        {
            cnx = new SqlConnection(WebConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            adapterProgramme = new SqlDataAdapter("select * from programme", cnx);
            adapterProgrammeDetails = new SqlDataAdapter("select * from programme_details", cnx);

            programmes = new DataTable();
            programme_details = new DataTable();
            try
            {
                cnx.Open();
                adapterProgramme.Fill(programmes);
                adapterProgrammeDetails.Fill(programme_details);
            }
#pragma warning disable CS0168 // La variable 'ex' est déclarée, mais jamais utilisée
            catch (Exception ex)
#pragma warning restore CS0168 // La variable 'ex' est déclarée, mais jamais utilisée
            {
                Response.Write("<script>alert('Un erreur est servenue lors de la connection a la base de donnée')</script>");
            }
            finally
            {
                cnx.Close();
            }

            showProgrameDetails(1);
            day1.Text = programmes.Rows[0]["jour"].ToString();
            day2.Text = programmes.Rows[1]["jour"].ToString();
            day3.Text = programmes.Rows[2]["jour"].ToString();
            day4.Text = programmes.Rows[3]["jour"].ToString();

            day1Date.Text = DateTime.Parse(programmes.Rows[0]["activityDate"].ToString()).ToString("dd-MMMM-yyyy");
            day2Date.Text = DateTime.Parse(programmes.Rows[1]["activityDate"].ToString()).ToString("dd-MMMM-yyyy");
            day3Date.Text = DateTime.Parse(programmes.Rows[2]["activityDate"].ToString()).ToString("dd-MMMM-yyyy");
            day4Date.Text = DateTime.Parse(programmes.Rows[3]["activityDate"].ToString()).ToString("dd-MMMM-yyyy");






        }

        [WebMethod]
        public static List<string> getData(int id)
        {
            DataRow[] row = programme_details.Select("idProgramme=" + id);
            string timeStartActivity = DateTime.Parse(row[0]["heure"].ToString()).ToString("HH:MM");
            string timeEndActivity = DateTime.Parse(row[row.Length - 1]["heure"].ToString()).ToString("HH:MM");
            string timeActivity = timeStartActivity + "-" + timeEndActivity;

            string titleActivity = programmes.Select("idProgramme="+id)[0]["activityNom"].ToString();

            List<string> ls = new List<string>();
            ls.Add(timeActivity);
            ls.Add(titleActivity);
            for (int i = 0; i < row.Length; i++)
            {
                ls.Add("<p class='activity-time'><span class='time-details'>" + row[i]["heure"] + "</span>: <span class='titre-details'>" + row[i]["titre"] + "</span></p>");
            }

            return ls;
        }

        private void  showProgrameDetails(int id)
        {
            DataRow[] row = programme_details.Select("idProgramme=" + id);
            string timeStartActivity = DateTime.Parse(row[0]["heure"].ToString()).ToString("HH:MM");
            string timeEndActivity = DateTime.Parse(row[row.Length-1]["heure"].ToString()).ToString("HH:MM");
            TimeActivity.InnerText = timeStartActivity + "-" + timeEndActivity;
            TitleActivity.InnerText = programmes.Rows[0]["activityNom"].ToString();

            for (int i = 0; i < row.Length; i++)
            {
                myDiv.InnerHtml += "<p class='activity-time'><span class='time-details'>" + row[i]["heure"] + "</span>: <span class='titre-details'>" + row[i]["titre"] + "</span></p>";
            }

        }

        

    }
}
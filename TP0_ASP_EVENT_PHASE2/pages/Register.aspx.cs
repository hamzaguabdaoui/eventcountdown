﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TP0_ASP_EVENT_PHASE2.pages
{
    public partial class Register1 : System.Web.UI.Page
    {

        SqlConnection connection;
        protected void Page_Load(object sender, EventArgs e)
        {
            connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connection"].ConnectionString);
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                string imageName = DateTime.Now.ToBinary() +""+ upImage.FileName;
                int res = 0;
                string strQuery = "insert into registrations values(@nom,@email,@tel,@password,@image,@role)";
                SqlParameter[] parameters =
                {
                    new SqlParameter("nom",txtNom.Value),
                    new SqlParameter("email",txtEmail.Value),
                    new SqlParameter("tel",txtTel.Value),
                    new SqlParameter("password",txtPassword.Value),
                    new SqlParameter("image",imageName),
                    new SqlParameter("role","client")
                };
                SqlCommand cmd = new SqlCommand(strQuery, connection);
                cmd.Parameters.AddRange(parameters);
                try
                {
                    connection.Open();
                    //verification si l'email exist déja dans la base de donnée
                    string strCheck = "select count(email) from registrations where email=@email";
                    SqlCommand cmdCheck = new SqlCommand(strCheck, connection);
                    cmdCheck.Parameters.Add(new SqlParameter("@email", txtEmail.Value));
                    int nbrEmail = int.Parse(cmdCheck.ExecuteScalar().ToString());

                    if (nbrEmail == 0)
                    {
                        res=cmd.ExecuteNonQuery();
                        //<div class="alert-success">vous inscription est fait avec succès 
                        //vous recevrez un email de confirmation sur votre boit email</div>
                        //EnvoyerConfirmationMail(txtEmail.Text, txtNom.Text);
                        string response = "<script>" +
                                                 "alert( 'vous inscription est fait avec succès," +
                                                 "vous recevrez un email de confirmation sur votre boit email');" +
                                          "</script>";

                        Response.Write(response);
                    }
                    else
                    {
                        Response.Write("<script>a:alert('cet email exist deja')</script>");
                    }

                }
                catch (Exception EX)
                {
                    Response.Write("<script>alert('" + EX.Message + "')</script>");
                }
                finally
                {
                    connection.Close();
                    if (res != 0)
                        upImage.SaveAs(Server.MapPath( "..\\ProfileImages\\" + imageName));
                }
            }
        }

        protected void imageValidat_ServerValidate(object source, ServerValidateEventArgs args)
        {
            CustomValidator validator = ((CustomValidator)source);
            args.IsValid = false;
            string[] ext = {".jpeg",".jpg",".png",".gif",".bmp" };
            if (upImage.HasFile)
                if (upImage.PostedFile.ContentLength < 3 * 10e+6)
                    if (ext.Contains(Path.GetExtension(upImage.FileName)))
                    {
                        args.IsValid = true;
                    }
                    else
                        validator.ErrorMessage = "les fichier n'est pas une image valide";
            else
                    validator.ErrorMessage = "Image trop large ";
            else
                validator.ErrorMessage= "champs obligatoir";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TP0_ASP_EVENT_PHASE2.pages
{
    public partial class Invitation_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["client"] == null)
                Response.Redirect("Login.aspx");
        }
    }
}
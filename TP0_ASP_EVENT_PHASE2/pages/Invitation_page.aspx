﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Invitation_page.aspx.cs" Inherits="TP0_ASP_EVENT_PHASE2.pages.Invitation_page" %>


<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bienvenue - Agora ACI 2019 </title>

    <!-- Bootstrap core CSS -->
    <link href="" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="../css/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="../css/invitation.css" rel="stylesheet">
</head>

<body>

    <div class="overlay"></div>
    

    <div class="masthead">
        <div class="masthead-bg"></div>
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 my-auto">
                    <div class="masthead-content text-white py-5 py-md-0">
                        <h1 class="mb-3">Bienvenu dans Agora Conference</h1>
                        <p class="mb-5">
                            vous étes le bienvenu le 03  
             
                            <strong>october 2019</strong>! veullez imprimer cette page et de la presenter a l'entree de l'event.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
    <!-- Bootstrap core JavaScript -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Custom scripts for this template -->
    <script src="../js/coming-soon.js"></script>

</html>


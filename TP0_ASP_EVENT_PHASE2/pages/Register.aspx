﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/pages/layout.Master" CodeBehind="Register.aspx.cs" Inherits="TP0_ASP_EVENT_PHASE2.pages.Register1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <link href="../css/register.css" rel="stylesheet" />
</asp:Content>

<asp:Content ContentPlaceHolderID="bodyContent" runat="server">
    <div class="container">
        <div id="register_form" role="form">
            <h2>Registration</h2>
        <div class="form-group">
            <label for="firstName" class="col-sm-3 control-label">First Name</label>
            <div class="col-sm-9">
                <input type="text" runat="server" id="txtNom" placeholder="First Name" class="form-control" autofocus>
            </div>
        </div>
        <asp:RequiredFieldValidator ForeColor="Red" ErrorMessage="*ce champs est obligatoir" ControlToValidate="txtNom" runat="server" />
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">Email* </label>
            <div class="col-sm-9">
                <input type="email" runat="server" id="txtEmail" placeholder="Email" class="form-control" name="email">
            </div>
        </div>
        <asp:RegularExpressionValidator runat="server" 
            ControlToValidate="txtEmail"
            ErrorMessage="*l'email n'est pas valide \n exemple:test@test.ma"
            ForeColor="Red"
            ValidationExpression="^([A-Za-z0-9-_.]){2,}\@([A-Za-z]){4,}\.([A-Za-z]){2,4}$"
            />
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">Confirm email* </label>
            <div class="col-sm-9">
                <input type="email" runat="server" id="txtCofirmeEmail" placeholder="Email" class="form-control" name="email">
            </div>
        </div>
            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToValidate="txtCofirmeEmail" ControlToCompare="txtEmail" 
                    ErrorMessage="*les deux email doivent etre identique"></asp:CompareValidator>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Password*</label>
            <div class="col-sm-9">
                <input type="password" runat="server" id="txtPassword" placeholder="Password" class="form-control">
            </div>
        </div>
        <asp:RegularExpressionValidator ErrorMessage="*Mot passe invalid il doivent avoir au moin 8 caractères avec un majiscule,miniscule et un caracter spéciale" 
                        ControlToValidate="txtPassword"
                        ForeColor="Red"
                        runat="server"
                        ValidationExpression="(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9!:;?*@~#]{8,}"/>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Confirm Password*</label>
            <div class="col-sm-9">
                <input type="password" runat="server" id="txtConfirmePassword" placeholder="Password" class="form-control">
            </div>
        </div>
             <asp:CompareValidator runat="server"
            ControlToCompare="txtPassword"
            ControlToValidate="txtConfirmePassword"
            ErrorMessage="*Les deux mot de passe doivent se correspondre"
            ForeColor="Red"/>
        <div class="form-group">
            <label for="tel" class="col-sm-3 control-label">Phone number*</label>
            <div class="col-sm-9">
                <input type="tel" runat="server" id="txtTel" placeholder="Phone Number" class="form-control">
            </div>
        </div>
        <asp:RegularExpressionValidator runat="server"
            ControlToValidate="txtTel"
            ValidationExpression="^\+[0-9]{10,20}"
            ErrorMessage="*format tel invalide exemple:+212617879865"
            ForeColor="Red"/>
        <div class="form-group">
            <label for="txtProfession" class="col-sm-3 control-label">Profession*</label>
            <div class="col-sm-9">
                <input type="password" runat="server" id="txtProfession" placeholder="Profession" class="form-control">
            </div>
        </div>     
       <asp:RequiredFieldValidator ControlToValidate="txtProfession" runat="server" ErrorMessage="ce champs est obligatoir" ForeColor="Red"/>
        <div class="form-group">
            <label for="image" class="col-sm-3 control-label">Profile Image*</label>
            <div class="col-sm-9">
                    <asp:FileUpload runat="server" ID="upImage" />        
            </div>
            <asp:CustomValidator ErrorMessage="errormessage" ForeColor="Red" ControlToValidate="upImage"  ClientValidationFunction="imageValidate"  runat="server" ID="imageValidat"  OnServerValidate="imageValidat_ServerValidate"/>
            <asp:RequiredFieldValidator ErrorMessage="Image required" ControlToValidate="upImage" runat="server" />
        </div>
        <asp:Button ID="btnRegister" Text="Register" CssClass="btn btn-primary btn-block" OnClick="btnRegister_Click"  runat="server" />
        
        </div>
        
    </div>
    <!-- ./container -->
</asp:Content>

<asp:Content ContentPlaceHolderID="scripts" runat="server">
    <script>
        $(document).ready(onReady);
        function onReady() {
            
                    
            
        }
        $('#bodyContent_upImage').click(function () {
                 console.log('hggjgjgj')
            })

        

            function imageValidate(source, arg) {
                var ext = ["jpg", "jpeg", "png", "bmp", "gif"];
                let file = document.getElementById('<%=upImage.ClientID %>');
                arg.IsValid = false;
                if (file.files.length > 0)
                    if (file.files[0].size < 3000000)
                        if (ext.includes(file.files[0].name.split('.').pop())) {
                            arg.IsValid = true;
                        } else {
                            source.innerText = "les fichier n'est pas une image valide";
                        }
                    else {
                        source.innerText = "Image trop large ";
                    }
                else {
                    source.innerText = "champs obligatoir";
                }
            }
        

            console.log(source)
    </script>
    
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TP0_ASP_EVENT_PHASE2
{
    public partial class admin_master : System.Web.UI.MasterPage
    {
        SqlConnection connection;
        protected void Page_Load(object sender, EventArgs e)
        {


            string strCnx = WebConfigurationManager.ConnectionStrings["connection"].ConnectionString;
            connection = new SqlConnection(strCnx);
            SqlCommand cmd = new SqlCommand();
            string userInfo = "select nom,email,profileImage from registrations where id=@id and roles='admin'";
            SqlCommand user = new SqlCommand(userInfo,connection);
            user.Parameters.Add(new SqlParameter("id", Session["admin"].ToString()));
            cmd.CommandText = "select * from messages where status=0";
            cmd.Connection = connection;
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            int cpt = 0;
            while (reader.Read())
            {
                cpt++;
                string message = (reader["sujet"].ToString()).Substring(0, 30);
                message += "...";
                string row = "<div class='email__item' data-id='"+reader["idMessage"]+"'>" +
                                "<div class='image img-cir img-40'>"+
                                    "<img src='images/icon/avatar-06.jpg' alt='Image' />"+
                                "</div>" +
                                "<div class='content'>" +
                                   "<p>"+cpt+" "+message+"</p>" +
                                   "<span>"+reader["nom"]+"</span>" +
                                "</div>" +
                             "</div>";
                messageContent.InnerHtml += row;
            }
            quantity.InnerHtml = cpt.ToString();
            txtCountMessage.InnerHtml = cpt.ToString();
            reader.Close();
            //user details
            SqlDataReader userReader = user.ExecuteReader();
            userReader.Read();
            adminNom.InnerHtml = userReader["nom"].ToString();
            profileImg.Src = "ProfileImages/" + userReader["profileImage"].ToString();
            profileImg.Alt= userReader["nom"].ToString();
            secondImage.Src = "ProfileImages/" + userReader["profileImage"].ToString();
            secondImage.Alt= userReader["nom"].ToString();
            adminEmail.InnerHtml= userReader["email"].ToString();
            secondAdminNom.InnerHtml= userReader["nom"].ToString();
            userReader.Close();
            connection.Close();

            
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session["admin"] = null;
            Response.Redirect("Login.aspx");
        }
    }
}




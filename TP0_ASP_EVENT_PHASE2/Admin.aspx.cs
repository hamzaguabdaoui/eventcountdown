﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TP0_ASP_EVENT_PHASE2
{
    public partial class Admin : System.Web.UI.Page
    {
        static SqlConnection connection;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                Response.Redirect("Login.aspx");
                
            }
            string strCnx = WebConfigurationManager.ConnectionStrings["connection"].ConnectionString;
            connection = new SqlConnection(strCnx);
            SqlCommand getRegistrationCount = new SqlCommand("select count(id) from registrations", connection);
            SqlCommand getLatestRegistration = new SqlCommand("select top 3 *from registrations where roles='client' order by id desc", connection);
            connection.Open();
            int cpt = System.Convert.ToInt32(getRegistrationCount.ExecuteScalar());
            SqlDataReader reader = getLatestRegistration.ExecuteReader();
            while (reader.Read())
            {
                string row = "" +
                    " <div class='d-flex bd-highlight user-card'>" +
                        "<div class='img_cont'>" +
                            "<img src='ProfileImages/" + reader["profileImage"] + "'>" +
                            "</div>" +
                        "<div class='user_info'>" +
                            "<span>" + reader["nom"] + "</span>" +
                            "<p>" + reader["email"] + "</p>" +
                        "</div>" +
                        " <div class='user_date_register'>" +
                            "<p>12/08/2011</p>" +
                        "</div>" +
                    "</div><hr/>";

                latestRegistrations.InnerHtml += row;
            }
            connection.Close();
            txtTotalUsers.InnerText = Application["total_visiteurs"].ToString();
            currentVisits.InnerText = Application["visiteurs_online"].ToString();
            totalRegistration.InnerText = cpt.ToString();


        }

        [WebMethod]
        public static string[] getReservaitionStatistiques()
        {
            string[] data = new string[2];
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "select total,reserver from place";
            
            connection.Open();
            SqlDataReader reader= cmd.ExecuteReader();

            reader.Read();
            data[0] = reader[0].ToString();
            data[1] = reader[1].ToString();
            connection.Close();

            

            return data;
        }

        [WebMethod]
        public static List<string[]> getMessages()
        {
            List<string[]> data = new List<string[]>();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select top 5 * from messages order by idMessage desc";
            cmd.Connection = connection;
            try
            {
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string[] str = new string[reader.FieldCount];
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        str[i] = reader[i].ToString();
                    }
                    data.Add(str);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            
            
            return data;
        }
    }
}
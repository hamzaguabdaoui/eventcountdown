﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TP0_ASP_EVENT_PHASE2
{
    public partial class Messages : System.Web.UI.Page
    {
        static SqlConnection connection;
        protected void Page_Load(object sender, EventArgs e)
        {
            string strCnx = WebConfigurationManager.ConnectionStrings["connection"].ConnectionString;
            connection = new SqlConnection(strCnx);
            if (Request["id"] != null)
            {
                int idMessage = int.Parse(Request["id"]);

                SqlCommand changeStatus = new SqlCommand();
                changeStatus.CommandText = "update messages set status=1 where idMessage=@id";
                changeStatus.Parameters.Add(new SqlParameter("id", idMessage));
                changeStatus.Connection = connection;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select * from messages where idMessage=@id";
                cmd.Parameters.Add(new SqlParameter("id", idMessage));
                cmd.Connection = connection;

                connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    reader.Read();
                    txtId.Value= reader["idMessage"].ToString();
                txtEmail.InnerHtml = reader["email"].ToString();
                    txtFrom.InnerHtml = reader["nom"].ToString();
                    txtObjet.InnerHtml = reader["objet"].ToString();
                    txtMessage.InnerHtml = reader["sujet"].ToString();
                reader.Close();
                changeStatus.ExecuteNonQuery();
                connection.Close();
            }
        }
        [WebMethod]
        public static int deleteMessage(int id)
        {
           
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "delete from messages where idMessage=@id";
            cmd.Parameters.Add(new SqlParameter("id", id));
            connection.Open();
                int res = cmd.ExecuteNonQuery();
            connection.Close();

            return res;
        }
    }
}
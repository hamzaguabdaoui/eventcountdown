﻿<%@ Page Language="C#" MasterPageFile="~/admin_master.Master" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="TP0_ASP_EVENT_PHASE2.Messages1" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-container">
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <p class="h4">Messages</p>
                    <table id="tableMessage" class="table table-inbox table-hover">
                        <tbody runat="server" id="messages">

                        </tbody>
                    </table>
                     <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright ©2019 Agora ACI.</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ContentPlaceHolderID="scripts" runat="server">
    <script>
        $(document).ready(onReady);
        function onReady() {
            $('#tableMessage').delegate('.rowC', 'click', function () {
                document.location.href = "ShowMessage.aspx?id=" + $(this).data('id');
            });
            $('#menu-dashboard').removeClass('active');
            $('#menu-messages').addClass('active');
        }
        
    </script>
</asp:Content>

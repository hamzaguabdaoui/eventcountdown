﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TP0_ASP_EVENT_PHASE2.classes
{
    public class LogIn
    {
        private string connection;

        public string Connection {
            get { return connection; }
            set{ connection=value; }
        }

        

        public LogIn()
        {

        }

        public LogIn(string connection)
        {
            this.connection = connection;
        }


        public string[] checkUser(string email,string password)
        {
            SqlDataReader reader;
            string[] res=null;
            SqlConnection connection = new SqlConnection(this.connection);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select id,roles,nom,email,profileImage from registrations where email=@email and motDePasse=@password";
            cmd.Parameters.Add(new SqlParameter("email", email));
            cmd.Parameters.Add(new SqlParameter("password", password));
            cmd.Connection = connection;

            try
            {
                connection.Open();
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    res = new string[5] { reader["id"].ToString(), reader["roles"].ToString(), reader["nom"].ToString(), reader["email"].ToString(),reader["profileImage"].ToString() };
                }
            }
            catch (Exception EX)
            {

                throw;
            }
            finally
            {
                connection.Close();
            }
            return res;
        }

    }
}
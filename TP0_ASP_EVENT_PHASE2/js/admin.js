﻿

$(document).ready(onPageReady);
function onPageReady() {
    //Dashboard sxript
    loadMessages();
    $('#tableMessages').delegate('#row', 'click', function () {
        document.location.href = "ShowMessage.aspx?id="+$(this).data('id');
    });
}

function loadMessages() {
    PageMethods.getMessages(onSuccess);
    function onSuccess(data) {
        let row = "";
        for (let i = 0; i < data.length; i++) {
            row += "<tr id='row' data-id='"+data[i][0]+"'>" +
                "<td>" + data[i][1] + "</td>" +
                "<td>" + data[i][2] + "</td>" +
                "<td>" + data[i][3] + "</td>" +
                "<td>" + data[i][4] + "</td>" +
                "<tr>";
        }
        let table = document.getElementById("tableMessages")
        table.innerHTML = row;

        //call of statistique function after messages loads
        loadStatistiques();
    }
}

function loadStatistiques() {
    PageMethods.getReservaitionStatistiques(onSuccess);
    function onSuccess(data) {
        var ctx = document.getElementById("percent-chart");
        if (ctx) {
            ctx.height = 280;
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    datasets: [
                        {
                            label: "My First dataset",
                            data: [data[0], data[1]],
                            backgroundColor: [
                                '#00b5e9',
                                '#fa4251'
                            ],
                            hoverBackgroundColor: [
                                '#00b5e9',
                                '#fa4251'
                            ],
                            borderWidth: [
                                0, 0
                            ],
                            hoverBorderColor: [
                                'transparent',
                                'transparent'
                            ]
                        }
                    ],
                    labels: [
                        'Places totale',
                        'Places Reservée'
                    ]
                },
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    cutoutPercentage: 55,
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    legend: {
                        display: false
                    },
                    tooltips: {
                        titleFontFamily: "Poppins",
                        xPadding: 15,
                        yPadding: 10,
                        caretPadding: 0,
                        bodyFontSize: 16
                    }
                }
            });
        }
    }
}


$(document).ready(function(){
	Ajuster();
	$('.carousel').carousel();
	$(".nav").find("a").click(function(e) {
    e.preventDefault();
    var section = $(this).attr("href");
    $("html, body").animate({
        scrollTop: $(section).offset().top
    });
});

});

window.addEventListener("scroll", function (event) {
        //Ajustement du Header
        var scroll = this.scrollY;
        var navbar = document.getElementsByClassName('navbar');
        if(window.innerWidth>768)
	        if (scroll > 80) {
	            navbar[0].style.transition = "1s";
	            navbar[0].style.padding = "10px 40px";
	            navbar[0].style.backgroundColor = '#2A2A2C';
	            navbar[0].style.boxShadow = "0px 0px 15px #060D15";

	        } else {
	            navbar[0].style.padding = "10px 40px";
	            navbar[0].style.backgroundColor = 'transparent';
	            navbar[0].style.boxShadow = "unset";
	        }
	       //Chargement des animmation
	      
    });

//Code count Down

var countDownDate = new Date("Oct 3, 2019 15:37:25").getTime();
var x = setInterval(function() {
var now = new Date().getTime();
var distance = countDownDate - now;
var days = Math.floor(distance / (1000 * 60 * 60 * 24));
var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  
  document.getElementById("Timer").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
    
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("Timer").innerHTML = "EXPIRED";
  }
}, 1000);


function Ajuster() {
		var width=window.innerWidth;
		if(width<768){
			$('.navbar-default').css({
				'background-color':'rgb(3,9,9,0.9)',
				'padding':'1% 5%',
				'width':'100%',
				'position':'none'
			});
		    $('#eventInfo h2').css({
				'font-size':'4.3vw'
		    });
		     $('.cover div h1').css({
				'font-size':'6vw'
		    });
		
		}else{
			$('.navbar-default').css({
				'background-color':'transparent',
				'padding':'4% 0%'
			});
			$('#eventInfo h2').css({
				'font-size':'2.3vw'
		    });
		    $('.cover div h1').css({
				'font-size':'5vw'
		    });
		}
	}




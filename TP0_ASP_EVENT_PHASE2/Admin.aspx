﻿<%@ Page Language="C#" MasterPageFile="~/admin_master.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="TP0_ASP_EVENT_PHASE2.Admin" %>


<asp:Content runat="server" ContentPlaceHolderID="head">
    <link href="css/main.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-5 bg-light users-stats">
                            <h4>User stats</h4>
                            <div class="panel bg-flat-color-1">
                                <div class="website-performance">
                                    <div class="row gutter">
                                        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                            <div class="performance">
                                                <h5>Total visits</h5>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                                            <div class="performance-stats">
                                                <h3 runat="server" id="txtTotalUsers" class="odometer odometer-auto-theme">1299</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel bg-flat-color-3">
                                <div class="website-performance">
                                    <div class="row gutter">
                                        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                            <div class="performance">
                                                <h5>Total Registrations</h5>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                                            <div class="performance-stats">
                                                <h3 runat="server" id="totalRegistration" class="odometer odometer-auto-theme">1299</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel bg-flat-color-5">
                                <div class="website-performance">
                                    <div class="row gutter">
                                        <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6">
                                            <div class="performance">
                                                <h5>Current visits</h5>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                                            <div class="performance-stats">
                                                <h3 runat="server" id="currentVisits" class="odometer odometer-auto-theme">1299</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="latestRegistrations" class="col-md-5 bg-light recent_registration">
                            <h4>Recent registrations</h4>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-5">
                                <h2 class="title-1 m-b-25">Places Statistiques</h2>
                                <div class="au-card chart-percent-card">
                                    <div>
                                        <div>
                                            <div class="percent-chart">
                                                <canvas id="percent-chart"></canvas>
                                            </div>
                                        </div>
                                        <div class="chart-note-wrap">
                                            <div class="chart-note mr-0 d-block">
                                                <span class="dot dot--blue"></span>
                                                <span>place disponible</span>
                                            </div>
                                            <div class="chart-note mr-0 d-block">
                                                <span class="dot dot--red"></span>
                                                <span>place reservé</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <h2 class="title-1 m-b-25">Recent messages</h2>
                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>Nom</th>
                                                <th>Email</th>
                                                <th>Objet</th>
                                                <th>Message</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tableMessages">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright ©2019 Agora ACI.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="scripts" runat="server">
    <script src="js/admin.js"></script>
    <script src="js/chart.js"></script>
</asp:Content>


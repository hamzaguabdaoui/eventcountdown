﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP0_ASP_EVENT_PHASE2.Models
{
    public class User : IdentityUser
    {
        public string nom;
        public string email;
        public string profileImage { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace TP0_ASP_EVENT_PHASE2.Models
{
    public class dbContext:DbContext
    {
        public dbContext()
        {
            this.Database.Connection.ConnectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            
        }
        public DbSet<User> Users { get; set; } // My domain models

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}